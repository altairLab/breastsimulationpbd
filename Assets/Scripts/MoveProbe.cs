﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class MoveProbe : MonoBehaviour {

    //Initial probe position
    Vector3 finalPosition;
    //Final probe position
    Vector3 initialPosition;
    //Start and stop probe movement
    bool startSimulation = false;


    void Start () {
        //Initialize probe position
        initialPosition = new Vector3(0.461f, 1.186f, 1.716f);
        transform.position = initialPosition;

        //Final position to move probe
        finalPosition = new Vector3(0.596f, 0.995f, 1.581f);
	}
	
	// Update is called once per frame
	void Update () {

        //Press B start simulation
        if (Input.GetKeyDown(KeyCode.B))
        {
            startSimulation = true;
        }
        //Press R reset probe position
        if (Input.GetKeyDown(KeyCode.R))
        {
            transform.position = initialPosition;
            startSimulation = false;
        }

        if (startSimulation)
        {
            //Move probe to final position
            transform.position = Vector3.MoveTowards(transform.position, finalPosition, 0.005f);
        }
    }
}
