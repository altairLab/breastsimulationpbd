# Breast Simulation PBD

<p align="center">
    <img width="800" src="teaser-pbd.png" />
</p>

BreastSimulationPBD is a Unity project featuring a simulation of breast deformation due to ultrasound probe pressure, as described in our [paper](https://link.springer.com/article/10.1007%2Fs11548-019-01997-z).
The position based dynamics (PBD) implementation provided by NVIDIA FleX is exploited to model breast behavior.


## How to Run

To run this demo you need to have Unity-64bit (version 2018.3.15f1 or later) installed on your system. 
Afterwards, you have to:

1. Clone or download this repo.
2. Start Unity.
3. Select 'Open Project' and select the root folder that you have just cloned.
4. Press 'Play' button.

These scene has been tested with the following configuration: 
- Windows 10 64-bit, Visual Studio 2017.

## Usage

Keyboard controls:
- Key B: Start simulation.
- Key R: Reset simulation.

## Video

[![Video](http://img.youtube.com/vi/5ImkRUZAB24/0.jpg)](http://www.youtube.com/watch?v=5ImkRUZAB24 "PBD-breast")

## References
Tagliabue, E., Dall'Alba, D., Magnabosco, E., Tenga, C., Peterlik, I., Fiorini, P.: Position-based modeling of lesion displacement in ultrasound-guided
breast biopsy. International journal of computer assisted radiology and surgery (2019)

Altair Robotics Lab -
University of Verona